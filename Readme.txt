Nano sat tracker

How to use it:

1- Clone the project on your directory: 
2- Push the firmaware on the arduino

3- How to use the soft?
    Launch terminal on the clone directory
    use the command:
         "source venv/bin/activate"
         "pip install ephem"

4- Configure the soft: TLE + lat/long/altitude + serial port( see on the arduino ide)
5- To start tracking: launch the command "python3 sat.py"


Licence:
CC-BY-SA
