
//firmware v0.1
//commadn model 120230 => Az = 120, EL  = 3230

#include <Servo.h>
char reception[6];  //tableau pour la réception des données (6 valeurs)

int AZpos=90;  
int ELpos=90;  // declare initial position of the servo
int servoDelay =15; 

Servo AZServo;
Servo ELServo;

void setup() {
   Serial.begin(9600);
   Serial.println("init");
   
   AZServo.attach(8);
   ELServo.attach(9);
   
   AZServo.write(0);
   ELServo.write(0);

}

void loop() {
   while (Serial.available()) { //tant que des données sont en attente
    for (int p=0;p<6;p++){ //on en lit 6
      char c=Serial.read(); // lecture
      reception[p]=c; //écriture dans le tableau
      delay(10); //petite attente
      Serial.print(c);
    }
    while (Serial.available()){ //s'il reste des données
      Serial.read(); //on les lit sans les stocker
    }
    decodage(reception); // on appelle la fonction de décodage
  }
  delay(1000);
  
  //debug
  Serial.print("test_comm_auto : ");
  Serial.print(reception);
  Serial.print("");
  
}

void decodage(char c[6]){
  //debug
  Serial.print("init_decodage, command = ");
  Serial.println(c);
  
  //init variable
  int az_angle = 0;
  int el_angle = 0;
  
  //convert azimuth serial data to angle
  int az_digit1 = c[0] - '0';
  az_digit1 = az_digit1 *100;
  int az_digit2 = c[1] - '0';
  az_digit2= az_digit2 *10;
  int az_digit3 = c[2] - '0';
  az_angle = az_digit1 + az_digit2 + az_digit3;
  //debug
  //Serial.print("az = ");
  //Serial.print(az_angle);
  //Serial.println(" az_stop ");
  
  //convert elevation serial data to angle
  int el_digit1 = c[3] - '0';
  el_digit1 = el_digit1 *100;
  int el_digit2 = c[4] - '0';
  el_digit2= el_digit2 *10;
  int el_digit3 = c[5] - '0';
  el_angle = el_digit1 + el_digit2 + el_digit3;
  //debug
 // Serial.print("el = ");
 // Serial.print(el_angle);
  //Serial.println(" el_stop ");
 
  //send angle command to the motor
  az_motor(az_angle);
  el_motor(el_angle);
}

void az_motor(int angle){
  Serial.print(angle);
  AZServo.write(angle);
}

void el_motor(int angle){
 Serial.print(angle);
 ELServo.write(angle);
 
}
