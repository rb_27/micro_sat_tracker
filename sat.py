import time
import datetime
from math import *
import ephem
import serial


# taken from: http://celestrak.com/NORAD/elements/cubesat.txt
tle = { "name": "Syracuse 3 ", \
            "l1": '1 28885U 05041B   18334.90118809 +.00000122 +00000-0 +00000-0 0  9991', \
            "l2": '2 28885 000.0160 321.3175 0002564 276.3169 203.5536 01.00268342048128'}

position = ("43.6", "1.4", "100") #latitude, longitude, elevation

class Tracker():

    def __init__(self, satellite, groundstation):

        self.groundstation = ephem.Observer()   #init ephem observer object
        self.groundstation.lat = groundstation[0]
        self.groundstation.lon = groundstation[1]
        self.groundstation.elevation = int(groundstation[2])

        self.satellite = ephem.readtle(satellite["name"], satellite["l1"], satellite["l2"])

    def set_epoch(self):
        ''' compute traking data '''
        self.groundstation.date = datetime.datetime.utcnow() #current utc time
        self.satellite.compute(self.groundstation) #compute tracking data

    def azimuth(self):
        ''' returns satellite azimuth in degrees '''
        az_deg = int(degrees(self.satellite.az))
        az_str = ""
        if az_deg < 100:
            az_str = "0" + str(az_deg)
        else:
            az_str = str(az_deg)
        return az_str

    def elevation(self):
        ''' returns satellite elevation in degrees '''
        ele_deg = int(degrees(self.satellite.alt))
        ele_str  = ""
        if ele_deg < 100:
            ele_str = "0" + str(ele_deg)
        else:
            ele_str = str(ele_deg)
        return ele_str

    def latitude(self):
        ''' returns satellite latitude in degrees '''
        return degrees(self.satellite.sublat)

    def longitude(self):
        ''' returns satellite longitude in degrees '''
        return degrees(self.satellite.sublong)

    def date(self):
        """return date """
        return self.groundstation.date

class Serial_api():
    """send data to the arduino"""
    def __init__(self):
        a=1
        #self.ser = serial.Serial('/dev/ttyACM0', 9600)
        #self.ser.write("test")

    def send_2_arduino(az, ele):
        """send az and el data to arduino"""
        if int(az) > 0:
            if int(ele) > 0:
                command = az +ele
                print("command = ", command)
                print(type(command))
                print("convert")
                command_in_byte = str.encode(command)
                print("command_byte = ", command_in_byte)
                print(type(command_in_byte))
                i = 0
                ser.write(command_in_byte)
        else:
            print("error, negative angle tracking")

print("Satellite tracker v0.1")
time.sleep(1)

tracker = Tracker(satellite=tle, groundstation=position)
tracker.set_epoch()
#i = 0
#ser = serial.Serial('/dev/ttyACM0', 9600)
print("az= ", tracker.azimuth(), "ele= ", tracker.elevation())


#while i <1:
    #tracker.set_epoch(time.time())
    #ser = serial.Serial('/dev/ttyACM0', 9600)
    #tracker.set_epoch()
    #print ("azimuth: ", int(tracker.azimuth()))
    #print ("elevation:  ", int(tracker.elevation()))
    #print("date_utc: ", tracker.date())
    #Serial_api.send_2_arduino(int(tracker.azimuth()), int(tracker.elevation()))
    ##Serial_api.send_2_arduino("110", "100")
    #print (ser.readline())

    #i = i + 1

#Serial_api.send_2_arduino("001", "001")
#print (ser.readline())
#Serial_api.send_2_arduino("180", "180")
#Serial_api.send_2_arduino(int(tracker.azimuth()), int(tracker.elevation()))
#Serial_api.send_2_arduino("180", "180")
#Serial_api.send_2_arduino("111", "111")
#ser.read()
#Serial_api.send_2_arduino("080", "143")
#Serial_api.send_2_arduino("001", "001")
#Serial_api.send_2_arduino("090", "003")
